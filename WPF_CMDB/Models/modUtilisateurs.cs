﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modUtilisateurs : modContext
    {
        public static Utilisateurs userCon;

        public IEnumerable<Utilisateurs> takeAll()
        {
            IEnumerable<Utilisateurs> req = from u in db.Utilisateurs
                                      select u;
            return req;
        }
    }
}
