﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    abstract class modContext
    {
        protected CMDBDataContext db;

        public modContext()
        {
            db = new CMDBDataContext();
        }
        public void submit()
        {
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(ex.Message, "Erreur");
            }
        }
    }
}
