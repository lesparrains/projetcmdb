﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modStatuts : modContext
    {
        public IEnumerable<Statuts> takeAll()
        {
            IEnumerable<Statuts> req = from s in db.Statuts
                                       select s;
            return req;
        }
    }
}
