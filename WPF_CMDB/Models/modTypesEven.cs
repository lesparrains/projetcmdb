﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modTypesEven : modContext
    {
        public IEnumerable<TypesEven> takeAll()
        {
            IEnumerable<TypesEven> req = from t in db.TypesEven
                                            select t;
            return req;
        }

        public void supprimer(object p)
        {
            TypesEven asup = (TypesEven)p;

            TypesEven typeEvent = (from c in db.TypesEven
                                        where c.idTypeEven == asup.idTypeEven
                                        select c).FirstOrDefault();
            try
            {
                db.TypesEven.DeleteOnSubmit(typeEvent);

                db.SubmitChanges();

                MessageBox.Show("La suppresion a réussi.");
            }
            catch (Exception e)
            {
                MessageBox.Show("La suppresion n'a pas réussi. " + e.Message);
            }

        }
    }
}
