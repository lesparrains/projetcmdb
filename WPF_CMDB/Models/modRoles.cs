﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modRoles : modContext
    {
        public IEnumerable<Roles> takeAll()
        {
            IEnumerable<Roles> req = from r in db.Roles
                                      select r;
            return req;
        }
    }
}
