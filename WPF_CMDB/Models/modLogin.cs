﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modLogin
    {
        private CMDBDataContext ddc;

        public modLogin()
        {
            ddc = new CMDBDataContext();
        }

        public Utilisateurs matchUser(String u, string pswd)
        {
            var result = from p in ddc.Utilisateurs
                         where u == p.adresseMail
                         select p;

            return result.FirstOrDefault();
           
        }

        public bool matchPwd(Utilisateurs u, String m)
        {
            if (u.mdp == m)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    }

