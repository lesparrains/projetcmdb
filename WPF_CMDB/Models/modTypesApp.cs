﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modTypesApp : modContext
    {
        public IEnumerable<TypesApp> takeAll()
        {
            IEnumerable<TypesApp> res = from t in db.TypesApp
                                        select t;
            return res;
        }
    }
}
