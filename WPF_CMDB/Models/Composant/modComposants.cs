﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Composant
{
    class modComposants : modContext
    {
        public IEnumerable<Composants> takeAll()
        {
            IEnumerable<Composants> res = from c in db.Composants
                                          select c;
            return res;
        }

        public IEnumerable<Composants> takeAll(object o)
        {
            TypesComposant tp = (TypesComposant)o;
            IEnumerable<Composants> req = from e in db.Composants
                                          where e.fkTypeComposant == tp.idTypeComposant
                                          select e;
            return req;
        }

        public IEnumerable<Composants> getCompoID(object o)
        {
            Composants tp = (Composants)o;
            IEnumerable<Composants> req = from c in db.Composants
                                          where c.idComposant == tp.idComposant
                                          select c;
            return req;
        }

        public void edit(string si, string newAdresseMac, string newNomReseau, string newAdresseIP, string newMarque,
                         string newModele, bool? newEtat, object newCompose, List<string> lst)
        {
            int id = int.Parse(si);
            Composants newfk = (Composants)newCompose;
            Composants compo = (from c in db.Composants
                                where c.idComposant == id
                                select c).FirstOrDefault();

            compo.adresseMac = newAdresseMac;
            compo.nomReseau = newNomReseau;
            compo.adresseIP = newAdresseIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            if (newfk != null)
            {
                compo.fkCompose = newfk.idComposant;
            }


            int i = 1;
            foreach (string s in lst)
            {
                compo.GetType().GetProperty("champ" + i).SetValue(compo, s);
                i++;
            }

            try
            {
                db.SubmitChanges();

                MessageBox.Show("L'édition a réussi");
            }
            catch (Exception e)
            {
                MessageBox.Show("L'édition n'a pas réussi. " + e.Message);
            }
        }

        public void add(string newAdresseMac, string newNomReseau, string newAdresseIP, string newMarque,
                         string newModele, bool? newEtat, object newType, object newCompose, List<string> lst)
        {
            TypesComposant newfkType = (TypesComposant)newType;
            Composants newfkCompose = (Composants)newCompose;
            Composants compo = new Composants();

            compo.adresseMac = newAdresseMac;
            compo.nomReseau = newNomReseau;
            compo.adresseIP = newAdresseIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.fkTypeComposant = newfkType.idTypeComposant;
            if (newfkCompose != null)
            {
                compo.fkCompose = newfkCompose.idComposant;
            }


            int i = 1;
            foreach (string s in lst)
            {
                compo.GetType().GetProperty("champ" + i).SetValue(compo, s);
                i++;
            }
            try
            {
                db.Composants.InsertOnSubmit(compo);
                db.SubmitChanges();

                MessageBox.Show("L'insertion a réussi");
            }
            catch (Exception e)
            {
                MessageBox.Show("L'insertion n'a pas réussi. " + e.Message);
            }
            
        }
        public void supprimer(object p)
        {
            Composants asup = (Composants)p;

            Composants compo = (from c in db.Composants
                                where c.idComposant == asup.idComposant
                                select c).FirstOrDefault();
            try
            {
                db.Composants.DeleteOnSubmit(compo);

                db.SubmitChanges();

                MessageBox.Show("La suppresion a réussi.");
            }
            catch (Exception e)
            {
                MessageBox.Show("La suppresion n'a pas réussi. " + e.Message);
            }

        }

        public void desactiver(object compoadesactiver)
        {
            Composants aDesactiver = (Composants)compoadesactiver;
            int id = aDesactiver.idComposant;
            Composants compo;

            compo = (from c in db.Composants
                     where c.idComposant == id
                     select c).FirstOrDefault();

            compo.etat = false;
            db.SubmitChanges();

            do
            {
                compo = (from c in db.Composants
                         where c.fkCompose == id && c.etat == true
                         select c).FirstOrDefault();
                if (compo != null)
                {
                    compo.etat = false;
                    db.SubmitChanges();
                }

            } while (compo != null);
        }
    }
}
