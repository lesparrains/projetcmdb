﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;


namespace WPF_CMDB.Models.Composant
{
    class modServeurs : modComposants
    {
        private Composants compo;

        public modServeurs ()
        {
            compo = new Composants();
        }
        new public IEnumerable<Composants> takeAll()
        {
            IEnumerable<Composants> res = from c in db.Composants
                                          where c.fkTypeComposant == 5
                                          select c;
            return res;
        }

        public void add(string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newCM, string newProc, string newCarteReseau, string newRam, string newDd,
            string newLect, string newCG, string newCS, string newDA, string newDMS, string newBoi,
            string newAlim, string newRole, object newFK)
        {
            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCM;
            compo.champ2 = newProc;
            compo.champ3 = newCarteReseau;
            compo.champ4 = newRam;
            compo.champ5 = newDd;
            compo.champ6 = newLect;
            compo.champ7 = newCG;
            compo.champ8 = newCS;
            compo.champ9 = newDA;
            compo.champ10 = newDMS;
            compo.champ11 = newBoi;
            compo.champ12 = newAlim;
            compo.champ13 = newRole;
            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 5;

            db.Composants.InsertOnSubmit(compo);
            db.SubmitChanges();
            
        }

        public void edit(string si, string newMac, string newReseau, string newIP, string newMarque,
           string newModele, bool? newEtat, string newCarteMere, string newProcesseur, string newCarteReseau,
           string newRAM, string newDD, string newLecteur, string newCarteGraphique, string newCarteSon, string newDateAchat,
           string newDateService, string newBoitier, string newAlimentation, string newRole, object newFK)
        {
            int id = int.Parse(si);
            Composants compo = (from c in db.Composants
                                where c.idComposant == id
                                select c).FirstOrDefault();

            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCarteMere;
            compo.champ2 = newProcesseur;
            compo.champ3 = newCarteReseau;
            compo.champ4 = newRAM;
            compo.champ5 = newDD;
            compo.champ7 = newLecteur;
            compo.champ8 = newCarteGraphique;
            compo.champ9 = newCarteSon;
            compo.champ10 = newDateAchat;
            compo.champ11 = newDateService;
            compo.champ12 = newBoitier;
            compo.champ13 = newAlimentation;
            compo.champ14 = newRole;

            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 5;
            db.SubmitChanges();
        }


    }
}
