﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Composant
{
    class modPCFixes : modComposants
    {
        private Composants compo;
        public modPCFixes()
        {
            compo = new Composants();
        }
        new public IEnumerable<Composants> takeAll()
        {
            IEnumerable<Composants> res = from c in db.Composants
                                          where c.fkTypeComposant == 2
                                          select c;
            return res;
        }

        public void add(string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newCM, string newProc, string newRam, string newDd,
            string newLect, string newCG, string newCS, string newDA, string newDMS, string newBoi,
            string newAlim, string newEcran, string newSouris, string newClavier, object newFK)
        {
            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCM;
            compo.champ2 = newProc;
            compo.champ3 = newRam;
            compo.champ4 = newDd;
            compo.champ5 = newLect;
            compo.champ6 = newCG;
            compo.champ7 = newCS;
            compo.champ8 = newDA;
            compo.champ9 = newDMS;
            compo.champ10 = newBoi;
            compo.champ11 = newAlim;
            compo.champ12 = newEcran;
            compo.champ13 = newSouris;
            compo.champ14 = newClavier;
            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 2;

            db.Composants.InsertOnSubmit(compo);
            db.SubmitChanges();
        }

        public void edit(string si, string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newCarteMere, string newProcesseur, string newCarteReseau,
            string newRAM, string newDD, string newLecteur, string newCarteGraphique, string newCarteSon, string newDateAchat,
            string newDateService, string newBoitier, string newAlimentation, string newEcran, string newSouris,
            string newClavier, object newFK)
        {
            int id = int.Parse(si);
            Composants compo = (from c in db.Composants
                                where c.idComposant == id
                                select c).FirstOrDefault();

            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCarteMere;
            compo.champ2 = newProcesseur;
            compo.champ3 = newCarteReseau;
            compo.champ4 = newRAM;
            compo.champ5 = newDD;
            compo.champ7 = newLecteur;
            compo.champ8 = newCarteGraphique;
            compo.champ9 = newCarteSon;
            compo.champ10 = newDateAchat;
            compo.champ11 = newDateService;
            compo.champ12 = newBoitier;
            compo.champ13 = newAlimentation;
            compo.champ14 = newEcran;
            compo.champ15 = newSouris;
            compo.champ15 = newSouris;
            compo.champ16 = newClavier;

            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 2;
            db.SubmitChanges();
        }
    }
}
