﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Composant
{
    class modSwitchs : modComposants
    {
        private Composants compo;
        public modSwitchs()
        {
            compo = new Composants();
        }
        new public IEnumerable<Composants> takeAll()
        {
            IEnumerable<Composants> res = from c in db.Composants
                                          where c.fkTypeComposant == 7
                                          select c;
            return res;
        }

        public void add(string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newPort, string newManag, string newTaille, 
            string newNorme, object newFK)
        {
            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newPort;
            compo.champ2 = newManag;
            compo.champ3 = newTaille;
            compo.champ4 = newNorme;
            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 7;

            db.Composants.InsertOnSubmit(compo);
            db.SubmitChanges();
        }

        public void edit(string si, string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newNbPorts, string newManag, string newTaille,
            string newNorme, object newFK)
        {
            int id = int.Parse(si);
            Composants compo = (from c in db.Composants
                                where c.idComposant == id
                                select c).FirstOrDefault();

            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newNbPorts;
            compo.champ2 = newManag;
            compo.champ3 = newTaille;
            compo.champ4 = newNorme;
            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 7;
            db.SubmitChanges();
        }
    }
}
