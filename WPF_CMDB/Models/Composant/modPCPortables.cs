﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Composant
{
    class modPCPortables : modComposants
    {
        private Composants compo;

        public modPCPortables()
        {
            compo = new Composants();
        }
        new public IEnumerable<Composants> takeAll()
        {
            IEnumerable<Composants> res = from c in db.Composants
                                          where c.fkTypeComposant == 3
                                          select c;
            return res;
        }

        public void add(string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newCM, string newProc, string newCR,
            string newRam, string newDD, string newLect, string newCG, string newCS, string newDA,
            string newDS, string newCRM, object newFK)
        {
            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCM;
            compo.champ2 = newProc;
            compo.champ3 = newCR;
            compo.champ4 = newRam;
            compo.champ5 = newDD;
            compo.champ6 = newLect;
            compo.champ7 = newCG;
            compo.champ8 = newCS;
            compo.champ9 = newDA;
            compo.champ10 = newDS;
            compo.champ11 = newCRM;
            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 3;

            db.Composants.InsertOnSubmit(compo);
            db.SubmitChanges();
        }

        public void edit(string si, string newMac, string newReseau, string newIP, string newMarque,
            string newModele, bool? newEtat, string newCarteMere, string newProcesseur, string newCarteReseau,
            string newRAM, string newDD, string newLecteur, string newCarteGraphique, string newCarteSon, string newDateAchat,
            string newDateService, string newCarteResMob, object newFK)
        {
            int id = int.Parse(si);
            Composants compo = (from c in db.Composants
                                where c.idComposant == id
                                select c).FirstOrDefault();

            Composants newCompose = (Composants)newFK;

            compo.adresseMac = newMac;
            compo.nomReseau = newReseau;
            compo.adresseIP = newIP;
            compo.marque = newMarque;
            compo.modele = newModele;
            compo.etat = newEtat;
            compo.champ1 = newCarteMere;
            compo.champ2 = newProcesseur;
            compo.champ3 = newCarteReseau;
            compo.champ4 = newRAM;
            compo.champ5 = newDD;
            compo.champ7 = newLecteur;
            compo.champ8 = newCarteGraphique;
            compo.champ9 = newCarteSon;
            compo.champ10 = newDateAchat;
            compo.champ11 = newDateService;
            compo.champ12 = newCarteResMob;

            if (newCompose != null)
            {
                compo.fkCompose = newCompose.idComposant;
            }
            compo.fkTypeComposant = 3;
            db.SubmitChanges();
        }
    }
}
