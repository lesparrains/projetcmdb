﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
   
    class modApplications : modContext
    {
        public IEnumerable<Applications> takeAll()
        {
            IEnumerable<Applications> res = from a in db.Applications
                                            select a;
            return res;
        }

        public void supprimer(object p)
        {
            Applications asup = (Applications) p;

            Applications appli = (from c in db.Applications
                                where c.idApplication == asup.idApplication
                                select c).FirstOrDefault();
            try
            {
                db.Applications.DeleteOnSubmit(appli);

                db.SubmitChanges();

                MessageBox.Show("La suppresion a réussi.");
            }
            catch (Exception e)
            {
                MessageBox.Show("La suppresion n'a pas réussi. " + e.Message);
            }

        }
    }
}
