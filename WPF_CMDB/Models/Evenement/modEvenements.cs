﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Evenement
{
    class modEvenements : modContext
    {
        public IEnumerable<Evenements> takeAll()
        {
            IEnumerable<Evenements> req = from e in db.Evenements
                                         select e;
            return req;
        }
        public IEnumerable<Evenements> takeAll(object o)
        {
            TypesEven tp = (TypesEven)o;
            IEnumerable<Evenements> req = from e in db.Evenements
                                          where e.fkTypeEven == tp.idTypeEven
                                          
                                          select e;
            return req;
        }

        public IEnumerable<Evenements> takeUser(Utilisateurs u)
        {
            Utilisateurs user = u;
            IEnumerable<Evenements> req = from e in db.Evenements
                                          where e.fkAttribueA == user.idUtilisateur && e.fkStatut != 2
                                          select e;
            return req;
        }

        public IEnumerable<Evenements> getEventID(object o)
        {
            Evenements tp = (Evenements)o;
            IEnumerable<Evenements> req = from e in db.Evenements
                                          where e.idEvenement == tp.idEvenement
                                          select e;
            return req;
        }
    }
}
