﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models.Evenement
{
    class modDemandes : modEvenements
    {
        private Evenements even;

        public modDemandes()
        {
            even = new Evenements();
        }
        new public IEnumerable<Evenements> takeAll()
        {
            IEnumerable<Evenements> req = from e in db.Evenements
                                          where e.fkTypeEven == 3
                                          select e;
            return req;
        }

        public void edit(string si, string newTitre, DateTime? newDateOuerture, DateTime? newDateFermeture, string newFichierJoint,
            object newFKPriorite, object newFKStatut, object newFKApplication, object newFKComposant, object newFKOuvertPour, 
            object newFKOuvertPar, object newFKAttribue, string newDescription)
        {
            int id = int.Parse(si);
            Evenements even = (from e in db.Evenements
                                where e.idEvenement == id
                                select e).FirstOrDefault();

            Priorites newPriorite = (Priorites)newFKPriorite;
            Statuts newStatuts = (Statuts)newFKStatut;
            Applications newApplication = (Applications)newFKApplication;
            Composants newComposant = (Composants)newFKComposant;
            Utilisateurs newOuvertPour = (Utilisateurs)newFKOuvertPour;
            Utilisateurs newOuvertPar = (Utilisateurs)newFKOuvertPar;
            Utilisateurs newAttribue = (Utilisateurs)newFKAttribue;

            even.description = newDescription;
            even.titre = newTitre;
            even.fichierJoint = newFichierJoint;

            if (newDateOuerture != null)
            {
                even.dateOuverture = (DateTime)newDateOuerture;
            }

            if (newDateFermeture != null)
            {
                even.DateFermeture = (DateTime)newDateFermeture;
            }

            if (newPriorite != null)
            {
                even.fkPriorite = newPriorite.idPriorite;
            }

            if (newStatuts != null)
            {
                even.fkStatut = newStatuts.idStatut;
            }

            if (newApplication != null)
            {
                even.fkApplication = newApplication.idApplication;
            }

            if (newComposant != null)
            {
                even.fkComposant = newComposant.idComposant;
            }

            if (newOuvertPour != null)
            {
                even.fkOuvertPour = newOuvertPour.idUtilisateur;
            }

            if (newOuvertPar != null)
            {
                even.fkOuvertPar = newOuvertPar.idUtilisateur;
            }

            if (newAttribue != null)
            {
                even.fkAttribueA = newAttribue.idUtilisateur;
            }
            
            even.fkTypeEven = 3;
            db.SubmitChanges();
        }

        public void add(string id, string newTitre, DateTime? newDateOuerture, DateTime? newDateFermeture, string newFichierJoint,
            object newFKPriorite, object newFKStatut, object newFKApplication, object newFKComposant, object newFKOuvertPour,
            object newFKOuvertPar, object newFKAttribue, string newDescription)
        {
            Priorites newPriorite = (Priorites)newFKPriorite;
            Statuts newStatuts = (Statuts)newFKStatut;
            Applications newApplication = (Applications)newFKApplication;
            Composants newComposant = (Composants)newFKComposant;
            Utilisateurs newOuvertpour = (Utilisateurs)newFKOuvertPour;
            Utilisateurs newOuvertPar = (Utilisateurs)newFKOuvertPar;
            Utilisateurs newAttribue = (Utilisateurs)newFKAttribue;

            even.description = newDescription;
            even.titre = newTitre;
            even.fichierJoint = newFichierJoint;

            if (newDateOuerture != null)
            {
                even.dateOuverture = (DateTime)newDateOuerture;
            }

            if (newDateFermeture != null)
            {
                even.DateFermeture = (DateTime)newDateFermeture;
            }

            if (newPriorite != null)
            {
                even.fkPriorite = newPriorite.idPriorite;
            }

            if (newStatuts != null)
            {
                even.fkStatut = newStatuts.idStatut;
            }

            if (newApplication != null)
            {
                even.fkApplication = newApplication.idApplication;
            }

            if (newComposant != null)
            {
                even.fkComposant = newComposant.idComposant;
            }

            if (newOuvertpour != null)
            {
                even.fkOuvertPour = newOuvertpour.idUtilisateur;
            }

            if (newOuvertPar != null)
            {
                even.fkOuvertPar = newOuvertPar.idUtilisateur;
            }

            if (newAttribue != null)
            {
                even.fkAttribueA = newAttribue.idUtilisateur;
            }

            even.fkTypeEven = 3;

            db.Evenements.InsertOnSubmit(even);
            db.SubmitChanges();
        }
    }
}
