﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modServices : modContext
    {
        public IEnumerable<Services> takeAll()
        {
            IEnumerable<Services> req = from s in db.Services
                                      select s;
            return req;
        }

        public void supprimer(object p)
        {
            Services asup = (Services)p;

            Services ser = (from c in db.Services
                                where c.idService == asup.idService                                select c).FirstOrDefault();
            try
            {
                db.Services.DeleteOnSubmit(ser);

                db.SubmitChanges();

                MessageBox.Show("La suppresion a réussi.");
            }
            catch (Exception e)
            {
                MessageBox.Show("La suppresion n'a pas réussi. " + e.Message);
            }

        }
    }


}
