﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modPriorites : modContext
    {
        public IEnumerable<Priorites> takeAll()
        {
            IEnumerable<Priorites> req = from p in db.Priorites
                                     select p;
            return req;
        }
    }
}
