﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;

namespace WPF_CMDB.Models
{
    class modTypesComposant : modContext
    {
        public IEnumerable<TypesComposant> takeAll()
        {
            IEnumerable<TypesComposant> req = from t in db.TypesComposant
                                        select t;
            return req;
        }

        public List<string> getLibelles(int i)
        {
            var types = (from m in db.TypesComposant
                         where m.idTypeComposant == i
                         select m);
            List<String> liste = new List<String>();
            foreach (var type in types)
            {
                liste.Add(type.libelleChamp1);
                liste.Add(type.libelleChamp2);
                liste.Add(type.libelleChamp3);
                liste.Add(type.libelleChamp4);
                liste.Add(type.libelleChamp5);
                liste.Add(type.libelleChamp6);
                liste.Add(type.libelleChamp7);
                liste.Add(type.libelleChamp8);
                liste.Add(type.libelleChamp9);
                liste.Add(type.libelleChamp10);
                liste.Add(type.libelleChamp11);
                liste.Add(type.libelleChamp12);
                liste.Add(type.libelleChamp13);
                liste.Add(type.libelleChamp14);
                liste.Add(type.libelleChamp15);
                liste.Add(type.libelleChamp16);
                liste.Add(type.libelleChamp17);
                liste.Add(type.libelleChamp18);
                liste.Add(type.libelleChamp19);
                liste.Add(type.libelleChamp20);
            }
            return liste;
        }

        public int getTypeCompo(object type)
        {
            TypesComposant newType = (TypesComposant)type;
            return newType.idTypeComposant;
        }

        public void supprimer(object p)
        {
            TypesComposant asup = (TypesComposant)p;

            TypesComposant typecompo = (from c in db.TypesComposant
                            where c.idTypeComposant == asup.idTypeComposant
                            select c).FirstOrDefault();
            try
            {
                db.TypesComposant.DeleteOnSubmit(typecompo);

                db.SubmitChanges();

                MessageBox.Show("La suppresion a réussi.");
            }
            catch (Exception e)
            {
                MessageBox.Show("La suppresion n'a pas réussi. " + e.Message);
            }

        }
    }
}
