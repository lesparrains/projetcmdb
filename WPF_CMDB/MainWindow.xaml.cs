﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_CMDB.Context;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Evenement;
using WPF_CMDB.Views;
using WPF_CMDB.Views.Composants;
using WPF_CMDB.Views.Composants.Imprimante;
using WPF_CMDB.Views.Composants.PCFixe;
using WPF_CMDB.Views.Composants.PCPortable;
using WPF_CMDB.Views.Composants.Routeur;
using WPF_CMDB.Views.Composants.Serveur;
using WPF_CMDB.Views.Composants.Smartphones;
using WPF_CMDB.Views.Composants.Switch;
using WPF_CMDB.Views.Composants.TelFixe;
using WPF_CMDB.Views.Evenement;
using WPF_CMDB.Views.Evenement.DemandeService;
using WPF_CMDB.Views.Evenement.Incident;
using WPF_CMDB.Views.Evenement.Probleme;

namespace WPF_CMDB
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        modEvenements events = new modEvenements();
        public MainWindow()
        {
            InitializeComponent();
            lblUser.Content = "Utilisateur connecté : " + modUtilisateurs.userCon.prenom + " " + modUtilisateurs.userCon.nom;

            if (modUtilisateurs.userCon.fkRole == 2)
            {
                mnitAdmin.Visibility = Visibility.Hidden;
            }

            lstEvenementsUser.ItemsSource = events.takeUser(modUtilisateurs.userCon);
        }


        private void mnitServices_Click(object sender, RoutedEventArgs e)
        {
            fenServices f = new fenServices();
            f.Show();
        }

        private void mnitRoles_Click(object sender, RoutedEventArgs e)
        {
            fenRoles f = new fenRoles();
            f.Show();
        }

        private void mnitUtilisateurs_Click(object sender, RoutedEventArgs e)
        {
            fenUtilisateurs f = new fenUtilisateurs();
            f.Show();
        }

        private void mnitApplication_Click(object sender, RoutedEventArgs e)
        {
            fenApplications f = new fenApplications();
            f.Show();
        }

        private void mnitComposantsDyn_Click(object sender, RoutedEventArgs e)
        {
            fenComposantListe f = new fenComposantListe();
            f.Show();
        }

        private void mnittypeapp_Click(object sender, RoutedEventArgs e)
        {
            fenTypesApp f = new fenTypesApp();
            f.Show();
        }

        private void mnitStatuts_Click(object sender, RoutedEventArgs e)
        {
            fenStatuts f = new fenStatuts();
            f.Show();
        }

        private void mnitEvent_Click(object sender, RoutedEventArgs e)
        {
            fenEventListe f = new fenEventListe();
            f.Show();
        }

        private void mnitPriorites_Click(object sender, RoutedEventArgs e)
        {
            fenPriorites f = new fenPriorites();
            f.Show();
        }

        private void mnitTypesComposant_Click(object sender, RoutedEventArgs e)
        {
            fenTypeComposants f = new fenTypeComposants();
            f.Show();
        }

        private void mnitTypesEvenements_Click(object sender, RoutedEventArgs e)
        {
            fenTypeEvenements f = new fenTypeEvenements();
            f.Show();
        }

        private void mnitPCFixe_Click(object sender, RoutedEventArgs e)
        {
            fenListePCFixe f = new fenListePCFixe();
            f.Show();
        }

        private void mnitSwitch_Click(object sender, RoutedEventArgs e)
        {
            fenListeSwitch f = new fenListeSwitch();
            f.Show();
        }

        private void mnitRouteur_Click(object sender, RoutedEventArgs e)
        {
            fenListeRouteur f = new fenListeRouteur();
            f.Show();
        }

        private void mnitTelFixe_Click(object sender, RoutedEventArgs e)
        {
            fenListeTelFixe f = new fenListeTelFixe();
            f.Show();
        }

        private void mnitPCPortable_Click(object sender, RoutedEventArgs e)
        {
            fenListePCPortable f = new fenListePCPortable();
            f.Show();
        }

        private void mnitServeur_Click(object sender, RoutedEventArgs e)
        {
            fenListeServeur f = new fenListeServeur();
            f.Show();
        }

        private void mnitDemandeService_Click(object sender, RoutedEventArgs e)
        {
            fenListeDemandeService f = new fenListeDemandeService();
            f.Show();
        }

        private void mnitIncident_Click(object sender, RoutedEventArgs e)
        {
            fenListeIncident f = new fenListeIncident();
            f.Show();
        }

        private void mnitImprimante_Click(object sender, RoutedEventArgs e)
        {
            fenListeImprimante f = new fenListeImprimante();
            f.Show();
        }

        private void mnitSmartphone_Click(object sender, RoutedEventArgs e)
        {
            fenListeSmartphones f = new fenListeSmartphones();
            f.Show();
        }

        private void mnitProbleme_Click(object sender, RoutedEventArgs e)
        {
            fenListeProbleme f = new fenListeProbleme();
            f.Show();
        }

        private void mnitQuitter_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
