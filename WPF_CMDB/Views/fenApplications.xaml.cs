﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenApplications.xaml
    /// </summary>
    public partial class fenApplications : Window
    {
        private modApplications apps;
        private modTypesApp typeapps;
        public fenApplications()
        {
            InitializeComponent();
            apps = new modApplications();
            typeapps = new modTypesApp();
            dgrdApplications.ItemsSource = apps.takeAll();
            dgrdCBTypeApp.ItemsSource = typeapps.takeAll();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            apps.submit();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSupprimer_Click(object sender, RoutedEventArgs e)
        {
            apps.supprimer(dgrdApplications.SelectedItem);

            dgrdApplications.ItemsSource = apps.takeAll();
        }
    }
}
