﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;
using WPF_CMDB.Views.Composants.FenêtreIndividuelle;

namespace WPF_CMDB.Views.Composants.Imprimante
{
    /// <summary>
    /// Logique d'interaction pour fenListeImprimante.xaml
    /// </summary>
    public partial class fenListeImprimante : Window
    {
        private modImprimantes composants;
        public fenListeImprimante()
        {
            InitializeComponent();
            composants = new modImprimantes();
            lstComposants.ItemsSource = composants.takeAll();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenImprimanteDetail f = new fenImprimanteDetail() { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenImprimanteDetail f = new fenImprimanteDetail();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
