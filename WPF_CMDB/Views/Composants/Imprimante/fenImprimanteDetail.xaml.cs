﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.FenêtreIndividuelle
{
    /// <summary>
    /// Logique d'interaction pour fenImprimanteDetail.xaml
    /// </summary>
    public partial class fenImprimanteDetail : Window
    {
        private modImprimantes compo;
        private modComposants composants;
        public fenImprimanteDetail()
        {
            InitializeComponent();
            compo = new modImprimantes();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtMFP.Text, txtCouleur.Text, cmBoxFKCompose.SelectedItem);
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text,txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtMFP.Text, txtCouleur.Text, cmBoxFKCompose.SelectedItem);
        }


    }
}
