﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Smartphones
{
    /// <summary>
    /// Logique d'interaction pour fenSmartphonesDetails.xaml
    /// </summary>
    public partial class fenSmartphonesDetails : Window
    {
        
        private modComposants composants;
        private modSmartphones compo;
        public fenSmartphonesDetails()
        {
            InitializeComponent();
            compo = new modSmartphones();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }

        // Ajouter un enregistrement
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNumero.Text, txtOS.Text, txtEcran.Text, cmBoxFKCompose.SelectedItem);

            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNumero.Text, txtOS.Text, txtEcran.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
