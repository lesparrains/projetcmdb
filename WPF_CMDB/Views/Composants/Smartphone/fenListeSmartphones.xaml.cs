﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Smartphones
{
    /// <summary>
    /// Logique d'interaction pour fenListeSmartphones.xaml
    /// </summary>
    public partial class fenListeSmartphones : Window
    {
        private modSmartphones composants;
        public fenListeSmartphones()
        {
            InitializeComponent();
            composants = new modSmartphones();
            lstComposants.ItemsSource = composants.takeAll();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenSmartphonesDetails f = new fenSmartphonesDetails() { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        // Ajouter
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenSmartphonesDetails f = new fenSmartphonesDetails();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
