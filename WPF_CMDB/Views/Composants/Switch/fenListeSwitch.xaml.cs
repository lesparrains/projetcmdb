﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;
using WPF_CMDB.Views.Composants.Switch;

namespace WPF_CMDB.Views.Composants.Switch
{
    /// <summary>
    /// Logique d'interaction pour fenListeSwitch.xaml
    /// </summary>
    public partial class fenListeSwitch : Window
    {
        private modSwitchs composants;
        public fenListeSwitch()
        {
            InitializeComponent();
            composants = new modSwitchs();
            lstComposants.ItemsSource = composants.takeAll();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenSwitchDetails f = new fenSwitchDetails() { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenSwitchDetails f = new fenSwitchDetails();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
