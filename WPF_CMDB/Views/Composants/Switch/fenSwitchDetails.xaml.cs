﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Switch
{
    /// <summary>
    /// Logique d'interaction pour SwitchDétails.xaml
    /// </summary>
    
public partial class fenSwitchDetails : Window
    {
        private modComposants composants;
        private modSwitchs compo;
        public fenSwitchDetails()
        {
            InitializeComponent();
            compo = new modSwitchs();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNbPorts.Text, txtManag.Text, txtTaille.Text, txtNorme.Text,cmBoxFKCompose.SelectedItem);
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNbPorts.Text, txtManag.Text, txtTaille.Text, txtNorme.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
