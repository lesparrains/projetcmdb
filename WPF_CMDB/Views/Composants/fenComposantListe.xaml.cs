﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Composant;
using WPF_CMDB.Views.Composants.FenêtreIndividuelle;

namespace WPF_CMDB.Views.Composants
{
    /// <summary>
    /// Logique d'interaction pour fenComposantTest.xaml
    /// </summary>
    public partial class fenComposantListe : Window
    {
        private modComposants composants;
        private modTypesComposant typeCompo;

        public fenComposantListe()
        {
            InitializeComponent();

            composants = new modComposants();
            typeCompo = new modTypesComposant();

            lstComposants.ItemsSource = composants.takeAll();
            cboTypeComposant.ItemsSource = typeCompo.takeAll();
        }

        private void cboTypeComposant_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lstComposants.ItemsSource = composants.takeAll(cboTypeComposant.SelectedValue);
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenDetailComposant f = new fenDetailComposant(false) { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenDetailComposant f = new fenDetailComposant(true);
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            composants.supprimer(lstComposants.SelectedItem);

            lstComposants.ItemsSource = composants.takeAll(cboTypeComposant.SelectedValue);
        }

        private void btnDes_Click(object sender, RoutedEventArgs e)
        {
            composants.desactiver(lstComposants.SelectedItem);

            lstComposants.ItemsSource = composants.takeAll(cboTypeComposant.SelectedValue);
        }

        
    }
}
