﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Serveur
{
    /// <summary>
    /// Logique d'interaction pour fenServeurDetails.xaml
    /// </summary>
    public partial class fenServeurDetails : Window
    {
        modComposants composants;
        private modServeurs compo;
        public fenServeurDetails()
        {
            InitializeComponent();
            compo = new modServeurs();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text, txtCarteReseau.Text,
                       txtRam.Text, txtDD.Text, txtLecteur.Text, txtCarteGraphique.Text, txtCarteSon.Text,
                       txtDateAchat.Text, txtDateService.Text, txtBoitier.Text, txtAlimentation.Text,
                       txtRole.Text, cmBoxFKCompose.SelectedItem);

            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text,
                       txtCarteReseau.Text, txtRam.Text, txtDD.Text, txtLecteur.Text,
                       txtCarteGraphique.Text, txtCarteSon.Text, txtDateAchat.Text,
                       txtDateService.Text, txtBoitier.Text, txtAlimentation.Text,
                       txtRole.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
