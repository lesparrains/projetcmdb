﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Serveur
{
    /// <summary>
    /// Logique d'interaction pour fenListeServeur.xaml
    /// </summary>
    public partial class fenListeServeur : Window
    {

        private modServeurs serveurs;
        public fenListeServeur()
        {
            InitializeComponent();
            serveurs = new modServeurs();
            lstComposants.ItemsSource = serveurs.takeAll();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenServeurDetails f = new fenServeurDetails();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenServeurDetails f = new fenServeurDetails() { DataContext = serveurs.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }
    }
}
