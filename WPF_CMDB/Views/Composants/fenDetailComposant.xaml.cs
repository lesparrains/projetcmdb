﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants
{
    /// <summary>
    /// Logique d'interaction pour fenDetailComposantTest.xaml
    /// </summary>
    public partial class fenDetailComposant : Window
    {
        modTypesComposant types;
        modComposants composants;
        List<string> listeLib;
        int nbChamps;
        int typeComp;
        int prevTypeComp = 0;
        bool action;

        public fenDetailComposant(bool act)
        {
            InitializeComponent();
            types = new modTypesComposant();
            composants = new modComposants();
            cmBoxFKTypeCompo.ItemsSource = types.takeAll();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
            action = act;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            generer(action);

            if (action)
            {
                cmBoxFKTypeCompo.IsEnabled = true;
                btnEdit.Visibility = Visibility.Hidden;
                btnInsert.Visibility = Visibility.Visible;
            }
 
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            btnEdit.Visibility = Visibility.Hidden;
            btnEnregistrer.Visibility = Visibility.Visible;

            showhide(true);
        }

        private void btnFermer_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            btnEdit.Visibility = Visibility.Visible;
            btnEnregistrer.Visibility = Visibility.Hidden;

            showhide(false);

            List<String> champs = new List<String>();

            for (int i = 1; i <= nbChamps; i++)
            {
                TextBox t = (TextBox)this.FindName("txtChamp" + i);
                champs.Add(t.Text);
            }

            composants.edit(txtID.Text, txtadresseMac.Text, txtnomReseau.Text, txtadresseIP.Text, txtmarque.Text, txtmodele.Text, etatCheckBox.IsChecked, cmBoxFKCompose.SelectedItem, champs);
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            if (cmBoxFKTypeCompo.SelectedItem != null)
            {
                List<String> champs = new List<String>();

                for (int i = 1; i <= nbChamps; i++)
                {
                    TextBox t = (TextBox)this.FindName("txtChamp" + i);
                    champs.Add(t.Text);
                }
                composants.add(txtadresseMac.Text, txtnomReseau.Text, txtadresseIP.Text, txtmarque.Text, txtmodele.Text, etatCheckBox.IsChecked, cmBoxFKTypeCompo.SelectedItem, cmBoxFKCompose.SelectedItem, champs);
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez spécifier un type de composant !");
            }
            
        }

        private void generer(bool a)
        {
            for (int i = 1; i <= prevTypeComp; i++)
            {
                TextBox t = (TextBox)this.FindName("txtChamp" + i);
                Label l = (Label)this.FindName("lblChamp" + i);
                t.Visibility = Visibility.Hidden;
                t.Text = String.Empty;
                l.Visibility = Visibility.Hidden;
            }

            if (cmBoxFKTypeCompo.SelectedItem != null)
            {
                typeComp = types.getTypeCompo(cmBoxFKTypeCompo.SelectedItem);
            }

            listeLib = new List<string>();

            listeLib = types.getLibelles(typeComp);

            nbChamps = 0;
            foreach (String s in listeLib)
            {
                if (s != null)
                {
                    nbChamps++;
                    TextBox t = (TextBox)this.FindName("txtChamp" + nbChamps);
                    Label l = (Label)this.FindName("lblChamp" + nbChamps);
                    t.Visibility = Visibility.Visible;
                    l.Visibility = Visibility.Visible;
                    l.Content = s;
                }
            }
            showhide(action);
        }

        private void showhide(bool etat)
        {
            txtadresseMac.IsEnabled = etat;
            txtnomReseau.IsEnabled = etat;
            txtadresseIP.IsEnabled = etat;
            txtmarque.IsEnabled = etat;
            txtmodele.IsEnabled = etat;
            etatCheckBox.IsEnabled = etat;
            cmBoxFKCompose.IsEnabled = etat;

            for (int i = 1; i <= nbChamps; i++)
            {
                TextBox t = (TextBox)this.FindName("txtChamp" + i);
                Label l = (Label)this.FindName("lblChamp" + i);
                t.IsEnabled = etat;
                t.Visibility = Visibility.Visible;
                l.Visibility = Visibility.Visible;
            }
        }

        private void cmBoxFKTypeCompo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            prevTypeComp = typeComp;
            for (int i = 1; i <= nbChamps; i++)
            {
                TextBox t = (TextBox)this.FindName("txtChamp" + i);
                Label l = (Label)this.FindName("lblChamp" + i);
                t.IsEnabled = false;
                t.Visibility = Visibility.Hidden;
                l.Visibility = Visibility.Hidden;

            }
            generer(action);
            
            showhide(true);
        }
    }
}
