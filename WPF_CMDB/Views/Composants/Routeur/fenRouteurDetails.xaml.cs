﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Routeur
{
    /// <summary>
    /// Logique d'interaction pour fenRouteurDetails.xaml
    /// </summary>
    public partial class fenRouteurDetails : Window
    {
        private modComposants composants;
        private modRouteurs compo;
        public fenRouteurDetails()
        {
            InitializeComponent();
            compo = new modRouteurs();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNbPorts.Text, cmBoxFKCompose.SelectedItem);
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtNbPorts.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
