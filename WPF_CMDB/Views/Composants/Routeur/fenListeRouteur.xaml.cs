﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.Routeur
{
    /// <summary>
    /// Logique d'interaction pour fenListeRouteur.xaml
    /// </summary>
    public partial class fenListeRouteur : Window
    {
        private modRouteurs composants;
        public fenListeRouteur()
        {
            InitializeComponent();
            composants = new modRouteurs();
            lstComposants.ItemsSource = composants.takeAll();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenRouteurDetails f = new fenRouteurDetails() { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenRouteurDetails f = new fenRouteurDetails();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
