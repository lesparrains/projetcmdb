﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.PCPortable
{
    /// <summary>
    /// Logique d'interaction pour fenPCPortableDetails.xaml
    /// </summary>
    public partial class fenPCPortableDetails : Window
    {
        private modComposants composants;
        private modPCPortables compo;
        public fenPCPortableDetails()
        {
            InitializeComponent();
            composants = new modComposants();
            compo = new modPCPortables();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text, txtReseau.Text,
                       txtRam.Text, txtDD.Text, txtLecteur.Text, txtCarteGraphique.Text, txtCarteSon.Text,
                       txtDateAchat.Text, txtDateService.Text, txtCarteResMob.Text,cmBoxFKCompose.SelectedItem);
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text, txtCarteReseau.Text, txtRam.Text, txtDD.Text, txtLecteur.Text,
                       txtCarteGraphique.Text, txtCarteSon.Text, txtDateAchat.Text, txtDateService.Text, txtCarteResMob.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
