﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.PCFixe
{
    /// <summary>
    /// Logique d'interaction pour fenListePCFixe.xaml
    /// </summary>
    /// 

   
    public partial class fenListePCFixe : Window
    {
        private modPCFixes composants;
        public fenListePCFixe()
        {
            InitializeComponent();

            composants = new modPCFixes();

            lstComposants.ItemsSource = composants.takeAll();
        }

        private void lstComposants_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenPCFixeDetails f = new fenPCFixeDetails() { DataContext = composants.getCompoID(lstComposants.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenPCFixeDetails f = new fenPCFixeDetails();
            f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
