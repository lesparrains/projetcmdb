﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Composants.PCFixe
{
    /// <summary>
    /// Logique d'interaction pour fenPCFixeDetails.xaml
    /// </summary>

    
    public partial class fenPCFixeDetails : Window
    {
        private modComposants composants;
        private modPCFixes compo;
        public fenPCFixeDetails()
        {
            InitializeComponent();
            compo = new modPCFixes();
            composants = new modComposants();
            cmBoxFKCompose.ItemsSource = composants.takeAll();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            compo.add(txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text, txtCarteReseau.Text,
                       txtRAM.Text, txtDD.Text, txtLecteur.Text, txtCarteGraphique.Text, txtCarteSon.Text,
                       txtDateAchat.Text, txtDateService.Text, txtBoitier.Text, txtAlimentation.Text,
                       txtSouris.Text, txtClavier.Text, cmBoxFKCompose.SelectedItem);
            this.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            compo.edit(txtID.Text, txtMac.Text, txtReseau.Text, txtIp.Text, txtMarque.Text, txtModele.Text,
                       cbEtat.IsChecked, txtCarteMere.Text, txtProcesseur.Text, txtCarteReseau.Text, txtRAM.Text, txtDD.Text, txtLecteur.Text,
                       txtCarteGraphique.Text, txtCarteSon.Text, txtDateAchat.Text, txtDateService.Text, txtBoitier.Text, txtAlimentation.Text,
                       txtEcran.Text, txtSouris.Text, txtClavier.Text, cmBoxFKCompose.SelectedItem);
        }
    }
}
