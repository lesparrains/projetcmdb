﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenServices.xaml
    /// </summary>
    public partial class fenRoles : Window
    {
        private modRoles roles;
        public fenRoles()
        {
            InitializeComponent();
            roles = new modRoles();
            dgrdRoles.ItemsSource = roles.takeAll();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            roles.submit();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
