﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Controlers.Evenement;
using WPF_CMDB.Context;
using WPF_CMDB.Controlers;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenLogin.xaml
    /// </summary>
    public partial class fenLogin : Window
    {
        private ctrLogin log;
        public fenLogin()
        {
            InitializeComponent();
            usernameTB.Focus();
        }

        private void LoginBTN_Click(object sender, RoutedEventArgs e)
        {
            log = new ctrLogin(usernameTB.Text, passwordBoxPB.Password, this);
        }

        private void passwordBoxPB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                log = new ctrLogin(usernameTB.Text, passwordBoxPB.Password, this);
            }
        }
    }
}
