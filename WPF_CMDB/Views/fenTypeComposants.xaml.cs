﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenTypeComposants.xaml
    /// </summary>
    public partial class fenTypeComposants : Window
    {
        private modTypesComposant typeComposant;

        public fenTypeComposants()
        {
           InitializeComponent();
           typeComposant = new modTypesComposant();
           dgrdTypesComposant.ItemsSource = typeComposant.takeAll();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            typeComposant.submit();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSupprimer_Click(object sender, RoutedEventArgs e)
        {
            typeComposant.supprimer(dgrdTypesComposant.SelectedItem);
            dgrdTypesComposant.ItemsSource = typeComposant.takeAll();
        }

    }
}
