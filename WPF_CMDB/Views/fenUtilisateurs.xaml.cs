﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenServices.xaml
    /// </summary>
    public partial class fenUtilisateurs : Window
    {
        modUtilisateurs users;
        modRoles roles;
        modServices services;
        modPCFixes pcfixes;
        modPCPortables pcportables;
        modTelephonesFixes telfixes;
        modSmartphones smartphones;
        
        public fenUtilisateurs()
        {
            InitializeComponent();
            users = new modUtilisateurs();
            roles = new modRoles();
            services = new modServices();
            pcfixes = new modPCFixes();
            pcportables = new modPCPortables();
            telfixes = new modTelephonesFixes();
            smartphones = new modSmartphones();

            dgrdUtilisateurs.ItemsSource = users.takeAll();
            dgrdCBRole.ItemsSource = roles.takeAll();
            dgrdCBService.ItemsSource = services.takeAll();
            dgrdCBPCFixe.ItemsSource = pcfixes.takeAll();
            dgrdCBPCPortable.ItemsSource = pcportables.takeAll();
            dgrdCBTelFixe.ItemsSource = telfixes.takeAll();
            dgrdCBTelPort.ItemsSource = smartphones.takeAll();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            users.submit();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
