﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Evenement;

namespace WPF_CMDB.Views.Evenement.Incident
{
    /// <summary>
    /// Logique d'interaction pour fenListeIncident.xaml
    /// </summary>
    public partial class fenListeIncident : Window
    {
        private modIncidents incident;
        public fenListeIncident()
        {
            InitializeComponent();
            incident = new modIncidents();
            lstEvenements.ItemsSource = incident.takeAll();
        }

        private void lstEvenements_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenDetailIncident f = new fenDetailIncident() { DataContext = incident.getEventID(lstEvenements.SelectedItem) };
            f.btnSave.Visibility = Visibility.Hidden;
            f.Show();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
          fenDetailIncident f = new fenDetailIncident();
          f.btnEdit.Visibility = Visibility.Hidden;
          f.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
