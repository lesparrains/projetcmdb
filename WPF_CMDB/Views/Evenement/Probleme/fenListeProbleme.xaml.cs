﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Evenement;

namespace WPF_CMDB.Views.Evenement.Probleme
{
    /// <summary>
    /// Logique d'interaction pour fenListeProbleme.xaml
    /// </summary>
    public partial class fenListeProbleme : Window
    {
        private modProblemes probleme;
        public fenListeProbleme()
        {
            InitializeComponent();
            probleme = new modProblemes();
            lstEvenements.ItemsSource = probleme.takeAll();
        }

        private void lstEvenements_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenDetailProbleme f = new fenDetailProbleme() { DataContext = probleme.getEventID(lstEvenements.SelectedItem) };
            f.Show();
            f.btnSave.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenDetailProbleme f = new fenDetailProbleme();
            f.Show();
            f.btnEdit.Visibility = Visibility.Hidden;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

      
    }
}
