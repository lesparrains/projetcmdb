﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Composant;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Evenement;
using Microsoft.Win32;

namespace WPF_CMDB.Views.Evenement.DemandeService
{
    /// <summary>
    /// Logique d'interaction pour fenDetailDemandeService.xaml
    /// </summary>
    public partial class fenDetailDemandeService : Window
    {
        private modDemandes demande;
        private modPriorites
            priorites;
        private modStatuts statuts;
        private modApplications applications;
        private modComposants composants;
        private modUtilisateurs users;
        public fenDetailDemandeService()
        {
            InitializeComponent();
            demande = new modDemandes();
            priorites = new modPriorites();
            statuts = new modStatuts();
            applications = new modApplications();
            composants = new modComposants();
            users = new modUtilisateurs();


            cboPriorite.ItemsSource = priorites.takeAll();
            cboStatut.ItemsSource = statuts.takeAll();
            cboApp.ItemsSource = applications.takeAll();
            cboComposant.ItemsSource = composants.takeAll();
            cboOuvertPour.ItemsSource = users.takeAll();
            cboOuvertPar.ItemsSource = users.takeAll();
            cboAttribueA.ItemsSource = users.takeAll();

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            demande.edit(idEvenementTextBox.Text,titreTextBox.Text,dateOuvertureDatePicker.SelectedDate, dateFermetureDatePicker.SelectedDate,
                fichierJointTextBox.Text,cboPriorite.SelectedItem, cboStatut.SelectedItem, cboApp.SelectedItem, cboComposant.SelectedItem,
            cboOuvertPour.SelectedItem, cboOuvertPar.SelectedItem, cboAttribueA.SelectedItem, descriptionTextBlock.Text);
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (cboPriorite.SelectedItem != null && cboStatut.SelectedItem != null && cboApp.SelectedItem != null && cboComposant.SelectedItem != null
               && cboOuvertPar.SelectedItem != null && cboOuvertPour.SelectedItem != null && cboAttribueA.SelectedItem != null)
            {
                demande.add(idEvenementTextBox.Text, titreTextBox.Text, dateOuvertureDatePicker.SelectedDate, dateFermetureDatePicker.SelectedDate,
                    fichierJointTextBox.Text, cboPriorite.SelectedItem, cboStatut.SelectedItem, cboApp.SelectedItem, cboComposant.SelectedItem,
                cboOuvertPour.SelectedItem, cboOuvertPar.SelectedItem, cboAttribueA.SelectedItem, descriptionTextBlock.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("Enregistrement non-valide.\n Certaines informations sont manquantes");
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string fichier;
            string targetPath = @"C:\Users\Jeremy\Documents\projetcmdb\WPF_CMDB\Fichiers joints";
            string destination;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            fichier = System.IO.Path.GetFileName(ofd.FileName);
            destination = System.IO.Path.Combine(targetPath, fichier);

            if (ofd.FileName != "")
            {
                System.IO.File.Copy(ofd.FileName, destination, true);
                fichierJointTextBox.Text = destination;
            }
        }
    }
}
