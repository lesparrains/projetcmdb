﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Evenement;

namespace WPF_CMDB.Views.Evenement.DemandeService
{
    /// <summary>
    /// Logique d'interaction pour fenListeDemandeService.xaml
    /// </summary>
    public partial class fenListeDemandeService : Window
    {
        private modDemandes demandes;
        public fenListeDemandeService()
        {
           InitializeComponent();
            demandes = new modDemandes();
           lstEvenements.ItemsSource = demandes.takeAll();
        }

        private void lstEvenements_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenDetailDemandeService f = new fenDetailDemandeService() { DataContext = demandes.getEventID(lstEvenements.SelectedItem) };
            f.btnSave.Visibility = Visibility.Hidden;
            f.Show();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            fenDetailDemandeService f = new fenDetailDemandeService();
            f.btnEdit.Visibility = Visibility.Hidden;
            f.Show();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
