﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;
using WPF_CMDB.Models.Composant;

namespace WPF_CMDB.Views.Evenement
{
    /// <summary>
    /// Logique d'interaction pour fenEventDetail.xaml
    /// </summary>
    public partial class fenEventDetail : Window
    {
        modTypesEven typesEvents;
        modPriorites priorites;
        modStatuts statuts;
        modApplications apps;
        modComposants comps;
        modUtilisateurs users;
        public fenEventDetail()
        {
            InitializeComponent();
            typesEvents = new modTypesEven();
            priorites = new modPriorites();
            statuts = new modStatuts();
            apps = new modApplications();
            comps = new modComposants();
            users = new modUtilisateurs();
            cboTypeEvent.ItemsSource = typesEvents.takeAll();
            cboPriorite.ItemsSource = priorites.takeAll();
            cboStatut.ItemsSource = statuts.takeAll();
            cboApp.ItemsSource = apps.takeAll();
            cboComposant.ItemsSource = comps.takeAll();
            cboOuvertPour.ItemsSource = users.takeAll();
            cboOuvertPar.ItemsSource = users.takeAll();
            cboAttribueA.ItemsSource = users.takeAll();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

          
        }

        

     
    }
}
