﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models.Evenement;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views.Evenement
{
    /// <summary>
    /// Logique d'interaction pour fenEventListe.xaml
    /// </summary>
    public partial class fenEventListe : Window
    {
        modEvenements events;
        modDemandes demandes;
        modIncidents incidents;
        modProblemes problemes;
        modTypesEven typesEvent;
        public fenEventListe()
        {
            InitializeComponent();
            events = new modEvenements();
            demandes = new modDemandes();
            incidents = new modIncidents();
            problemes = new modProblemes();
            typesEvent = new modTypesEven();

            lstEvenements.ItemsSource = events.takeAll();
            cboTypeEvent.ItemsSource = typesEvent.takeAll();
            
        }

        private void cboTypeEvent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lstEvenements.ItemsSource = events.takeAll(cboTypeEvent.SelectedValue);
        }

        private void lstEvenements_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            fenEventDetail f = new fenEventDetail { DataContext = events.getEventID(lstEvenements.SelectedItem) };
            f.Show();
        }

        

    }
}
