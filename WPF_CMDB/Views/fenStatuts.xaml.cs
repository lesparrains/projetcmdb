﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenStatuts.xaml
    /// </summary>
    public partial class fenStatuts : Window
    {
        modStatuts statuts;
        public fenStatuts()
        {
            InitializeComponent();
            statuts = new modStatuts();
            dgrdStatuts.ItemsSource = statuts.takeAll();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            statuts.submit();
        }
    }
}
