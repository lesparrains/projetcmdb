﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_CMDB.Models;

namespace WPF_CMDB.Views
{
    /// <summary>
    /// Logique d'interaction pour fenPriorites.xaml
    /// </summary>
    public partial class fenPriorites : Window
    {
        private modPriorites priorite;
        public fenPriorites()
        {
            InitializeComponent();
            priorite = new modPriorites();
            dgrdPriorites.ItemsSource = priorite.takeAll();
        }

        private void btnEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            priorite.submit();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
