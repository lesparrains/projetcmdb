﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPF_CMDB.Context;
using WPF_CMDB.Models;

namespace WPF_CMDB.Controlers.Evenement
{
    class ctrLogin
    {
        modLogin m = new modLogin();
        Utilisateurs user;

        public ctrLogin(String u, String p, Views.fenLogin fenLogin)
        {
            user = m.matchUser(u, p);
            if (user != null && m.matchPwd(user, p))
            {
                if (user.fkRole == 3)
                {
                    MessageBox.Show("Cette application est seulement disponible pour les techniciens.");
                }
                else
                {
                    modUtilisateurs.userCon = user;
                    MessageBox.Show("Bienvenue " + modUtilisateurs.userCon.prenom + " " + modUtilisateurs.userCon.nom);
                    MainWindow f = new MainWindow();
                    f.Show();
                    fenLogin.Close();
                }
            }
            else
            {
                MessageBox.Show("Utilisateur ou mot de passe incorrect");
            }
        }
    }
}

