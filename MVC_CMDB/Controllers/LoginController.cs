﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MVC_CMDB.Models;

namespace MVC_CMDB.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Log
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogIn(modLogin user)
        {
            if (ModelState.IsValid)
            {
                int role = IsValid(user.AdresseMail, user.MotDePasse);
                if (role != 0)
                {
                    FormsAuthentication.SetAuthCookie(role + user.AdresseMail, true);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Informations de connexion fausses.");
                }
            }
            return View();
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
        private int IsValid(String AdresseMail, String MotDePasse)
        {
            int isValid = 0;
            using (var db = new CMDBCMDBEntities())
            {
                var user = db.Utilisateurs.FirstOrDefault(u => u.adresseMail == AdresseMail);
                if (user != null)
                {
                    if (user.mdp == MotDePasse)
                    {
                        isValid = user.fkRole;
                    }
                }
            }
            return isValid;
        }
    }
}