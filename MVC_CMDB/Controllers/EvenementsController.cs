﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_CMDB.Models;

namespace MVC_CMDB.Controllers
{
    public class EvenementsController : Controller
    {
        private CMDBCMDBEntities db = new CMDBCMDBEntities();

        // GET: /Evenements/
        public ActionResult Index()
        {
            var evenements = db.Evenements.Include(e => e.Applications).Include(e => e.Composants).Include(e => e.Evenements2).Include(e => e.Priorites).Include(e => e.Statuts).Include(e => e.TypesEven).Include(e => e.Utilisateurs).Include(e => e.Utilisateurs1).Include(e => e.Utilisateurs2);
            return View(evenements.ToList());
        }

        // GET: /Evenements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evenements evenements = db.Evenements.Find(id);
            if (evenements == null)
            {
                return HttpNotFound();
            }
            return View(evenements);
        }

        // GET: /Evenements/Create
        public ActionResult Create()
        {
            ViewBag.fkApplication = new SelectList(db.Applications, "idApplication", "editeur");
            ViewBag.fkComposant = new SelectList(db.Composants, "idComposant", "adresseMac");
            ViewBag.fkIncidentAdj = new SelectList(db.Evenements, "idEvenement", "titre");
            ViewBag.fkPriorite = new SelectList(db.Priorites, "idPriorite", "priorite");
            ViewBag.fkStatut = new SelectList(db.Statuts, "idStatut", "statut");
            ViewBag.fkTypeEven = new SelectList(db.TypesEven, "idTypeEven", "typeEven");
            ViewBag.fkAttribueA = new SelectList(db.Utilisateurs, "idUtilisateur", "nom");
            ViewBag.fkOuvertPour = new SelectList(db.Utilisateurs, "idUtilisateur", "nom");
            ViewBag.fkOuvertPar = new SelectList(db.Utilisateurs, "idUtilisateur", "nom");
            return View();
        }

        // POST: /Evenements/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="idEvenement,titre,description,dateOuverture,DateFermeture,fichierJoint,fkTypeEven,fkPriorite,fkStatut,fkApplication,fkComposant,fkOuvertPour,fkOuvertPar,fkAttribueA,fkIncidentAdj")] Evenements evenements)
        {
            if (ModelState.IsValid)
            {
                db.Evenements.Add(evenements);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fkApplication = new SelectList(db.Applications, "idApplication", "editeur", evenements.fkApplication);
            ViewBag.fkComposant = new SelectList(db.Composants, "idComposant", "adresseMac", evenements.fkComposant);
            ViewBag.fkIncidentAdj = new SelectList(db.Evenements, "idEvenement", "titre", evenements.fkIncidentAdj);
            ViewBag.fkPriorite = new SelectList(db.Priorites, "idPriorite", "priorite", evenements.fkPriorite);
            ViewBag.fkStatut = new SelectList(db.Statuts, "idStatut", "statut", evenements.fkStatut);
            ViewBag.fkTypeEven = new SelectList(db.TypesEven, "idTypeEven", "typeEven", evenements.fkTypeEven);
            ViewBag.fkAttribueA = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkAttribueA);
            ViewBag.fkOuvertPour = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPour);
            ViewBag.fkOuvertPar = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPar);
            return View(evenements);
        }

        // GET: /Evenements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evenements evenements = db.Evenements.Find(id);
            if (evenements == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkApplication = new SelectList(db.Applications, "idApplication", "editeur", evenements.fkApplication);
            ViewBag.fkComposant = new SelectList(db.Composants, "idComposant", "adresseMac", evenements.fkComposant);
            ViewBag.fkIncidentAdj = new SelectList(db.Evenements, "idEvenement", "titre", evenements.fkIncidentAdj);
            ViewBag.fkPriorite = new SelectList(db.Priorites, "idPriorite", "priorite", evenements.fkPriorite);
            ViewBag.fkStatut = new SelectList(db.Statuts, "idStatut", "statut", evenements.fkStatut);
            ViewBag.fkTypeEven = new SelectList(db.TypesEven, "idTypeEven", "typeEven", evenements.fkTypeEven);
            ViewBag.fkAttribueA = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkAttribueA);
            ViewBag.fkOuvertPour = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPour);
            ViewBag.fkOuvertPar = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPar);
            return View(evenements);
        }

        // POST: /Evenements/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="idEvenement,titre,description,dateOuverture,DateFermeture,fichierJoint,fkTypeEven,fkPriorite,fkStatut,fkApplication,fkComposant,fkOuvertPour,fkOuvertPar,fkAttribueA,fkIncidentAdj")] Evenements evenements)
        {
            if (ModelState.IsValid)
            {
                db.Entry(evenements).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkApplication = new SelectList(db.Applications, "idApplication", "editeur", evenements.fkApplication);
            ViewBag.fkComposant = new SelectList(db.Composants, "idComposant", "adresseMac", evenements.fkComposant);
            ViewBag.fkIncidentAdj = new SelectList(db.Evenements, "idEvenement", "titre", evenements.fkIncidentAdj);
            ViewBag.fkPriorite = new SelectList(db.Priorites, "idPriorite", "priorite", evenements.fkPriorite);
            ViewBag.fkStatut = new SelectList(db.Statuts, "idStatut", "statut", evenements.fkStatut);
            ViewBag.fkTypeEven = new SelectList(db.TypesEven, "idTypeEven", "typeEven", evenements.fkTypeEven);
            ViewBag.fkAttribueA = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkAttribueA);
            ViewBag.fkOuvertPour = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPour);
            ViewBag.fkOuvertPar = new SelectList(db.Utilisateurs, "idUtilisateur", "nom", evenements.fkOuvertPar);
            return View(evenements);
        }

        // GET: /Evenements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evenements evenements = db.Evenements.Find(id);
            if (evenements == null)
            {
                return HttpNotFound();
            }
            return View(evenements);
        }

        // POST: /Evenements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Evenements evenements = db.Evenements.Find(id);
            db.Evenements.Remove(evenements);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
