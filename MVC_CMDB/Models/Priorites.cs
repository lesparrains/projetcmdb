//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_CMDB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Priorites
    {
        public Priorites()
        {
            this.Evenements = new HashSet<Evenements>();
        }
    
        public int idPriorite { get; set; }
        public string priorite { get; set; }
    
        public virtual ICollection<Evenements> Evenements { get; set; }
    }
}
