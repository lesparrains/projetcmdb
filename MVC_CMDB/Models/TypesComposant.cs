//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_CMDB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TypesComposant
    {
        public TypesComposant()
        {
            this.Composants = new HashSet<Composants>();
        }
    
        public int idTypeComposant { get; set; }
        public string typeComposant { get; set; }
        public string libelleChamp1 { get; set; }
        public string libelleChamp2 { get; set; }
        public string libelleChamp3 { get; set; }
        public string libelleChamp4 { get; set; }
        public string libelleChamp5 { get; set; }
        public string libelleChamp6 { get; set; }
        public string libelleChamp7 { get; set; }
        public string libelleChamp8 { get; set; }
        public string libelleChamp9 { get; set; }
        public string libelleChamp10 { get; set; }
        public string libelleChamp11 { get; set; }
        public string libelleChamp12 { get; set; }
        public string libelleChamp13 { get; set; }
        public string libelleChamp14 { get; set; }
        public string libelleChamp15 { get; set; }
        public string libelleChamp16 { get; set; }
        public string libelleChamp17 { get; set; }
        public string libelleChamp18 { get; set; }
        public string libelleChamp19 { get; set; }
        public string libelleChamp20 { get; set; }
    
        public virtual ICollection<Composants> Composants { get; set; }
    }
}
