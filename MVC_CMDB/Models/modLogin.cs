﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_CMDB.Models
{
    public class modLogin
    {
        [Required]
        [EmailAddress]
        [StringLength(128)]
        [Display(Name = "Adresse email : ")]
        public String AdresseMail { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Mot de passe : ")]
        public String MotDePasse { get; set; }
    }
}